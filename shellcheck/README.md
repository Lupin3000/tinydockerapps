# shellcheck

## Prepare shellcheck docker image

```shell
# change directory (optional)
$ cd ~/Projects/tinydockerapps

# build docker image for shellcheck
$ docker build -t alpine/shellcheck shellcheck/

# check available images (optional)
$ docker images
```

## Usage of shellcheck

_Note: Directory "~/Scripts/" contains shell script "example.sh"_

```shell
# list help
$ docker run --rm -i -v ~/Scripts/:/mnt alpine/shellcheck example.sh
```
