# exiftool

## Prepare exiftool docker image

```shell
# change directory (optional)
$ cd ~/Projects/tinydockerapps

# build docker image for exiftool
$ docker build -t debian/exiftool exiftool/

# check available images (optional)
$ docker images
```

## Usage of exiftool

_Note: Directory "~/Videos/" contains video file "demo.mp4"_

```shell
# show all parameters
$ docker run --rm -i -v ~/Videos/:/mnt debian/exiftool demo.mp4

# show all parameters sort by group (including duplicate and unknown tags)
$ docker run --rm -i -v ~/Videos/:/mnt debian/exiftool -a -u -g1 demo.mp4

# show friendly parameters
$ docker run --rm -i -v ~/Videos/:/mnt debian/exiftool -s -G demo.mp4

# show Height and Width
$ docker run --rm -i -v ~/Videos/:/mnt debian/exiftool '-*source*image*' demo.mp4

# show audio format
$ docker run --rm -i -v ~/Videos/:/mnt debian/exiftool '-*Audio*Format*' demo.mp4

# show video duration
$ docker run --rm -i -v ~/Videos/:/mnt debian/exiftool '-*Duration*' demo.mp4 | head -1
```
