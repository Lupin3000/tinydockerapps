# mplayer

## Prepare mplayer docker image

```shell
# change directory (optional)
$ cd ~/Projects/tinydockerapps

# build docker image for mplayer
$ docker build -t debian/mplayer mplayer/

# check available images (optional)
$ docker images
```

## Usage of mplayer

_Note: Directory "~/Videos/" contains video file "demo.mp4"_

```shell
# list help
$ docker run --rm -i -v ~/Videos/:/mnt debian/mplayer --help

# show all properties
$ docker run --rm -i -v ~/Videos/:/mnt debian/mplayer -vo null -ao null -frames 0 -identify demo.mp4

# show all video properties
$ docker run --rm -i -v ~/Videos/:/mnt debian/mplayer -vo null -ao null -frames 0 -identify movie.mov | grep VIDEO

# show all audio properties
$ docker run --rm -i -v ~/Videos/:/mnt debian/mplayer -vo null -ao null -frames 0 -identify movie.mov | grep AUDIO

# show video format
$ docker run --rm -i -v ~/Videos/:/mnt debian/mplayer -vo null -ao null -frames 0 -identify demo.mp4 | grep ID_VIDEO_FORMAT
```
