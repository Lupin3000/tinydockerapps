# avprobe

## Prepare avprobe docker image

```shell
# change directory (optional)
$ cd ~/Projects/tinydockerapps

# build docker image for avprobe
$ docker build -t debian/avprobe avprobe/

# check available images (optional)
$ docker images
```

## Usage of avprobe

_Note: Directory "~/Videos/" contains video file "demo.mp4"_

```shell
# list help
$ docker run --rm -i -v ~/Videos/:/mnt debian/avprobe --help

# show all properties
$ docker run --rm -i -v ~/Videos/:/mnt debian/avprobe demo.mp4

# show stream properties in json format
$ docker run --rm -i -v ~/Videos/:/mnt debian/avprobe -of json -loglevel quiet -show_streams demo.mp4

# show specific properties
$ docker run --rm -i -v ~/Videos/:/mnt debian/avprobe -show_format -show_streams -pretty demo.mp4

# show size properties
$ docker run --rm -i -v ~/Videos/:/mnt debian/avprobe -show_entries format=size demo.mp4

# show duration and size properties
$ docker run --rm -i -v ~/Videos/:/mnt debian/avprobe -loglevel quiet -show_entries format=duration,size demo.mp4

# show duration and size properties in json format
$ docker run --rm -i -v ~/Videos/:/mnt debian/avprobe -of json -loglevel quiet -show_entries format=duration,size demo.mp4
```
