# knocker

## Prepare knocker docker image

```shell
# change directory (optional)
$ cd ~/Projects/tinydockerapps

# build docker image for shellcheck
$ docker build -t alpine/knocker knocker/

# check available images (optional)
$ docker images
```

## Usage of knocker

```shell

# show help
$ docker run -ti --rm alpine/knocker --help

# run simple port scan
$ docker run -ti --rm alpine/knocker -H 192.168.192.1 -SP 1 -EP 10

# run port scan with specific ports and create report
$ docker run -ti --rm -v /home/docker:/results alpine/knocker -H 192.168.192.1 -SP 80 -EP 90 -lf /results/report

# read report
$ cat report
```
