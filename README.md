# My TinyDockerApps

collection of my own tiny docker files for applications

## Video tools

- [avprobe](/avprobe/README.md)
- [exiftool](/exiftool/README.md)
- [mediainfo](/mediainfo/README.md)
- [mplayer](/mplayer/README.md)

## Linter

- [shellcheck](/shellcheck/README.md)

## Security

- [knocker](/knocker/README.md)

# Prepare and clone

```shell
# create main project folder
$ mkdir ~/Projects/

# clone repository
$ git clone https://Lupin3000@bitbucket.org/Lupin3000/tinydockerapps.git ~/Projects/tinydockerapps

# change to directory
$ cd ~/Projects/tinydockerapps
```
