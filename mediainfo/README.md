# mediainfo

## Prepare mediainfo docker image

```shell
# change directory (optional)
$ cd ~/Projects/tinydockerapps

# build docker image for mediainfo
$ docker build -t debian/mediainfo mediainfo/

# check available images (optional)
$ docker images
```

## Usage of mediainfo

_Note: Directory "~/Videos/" contains video file "demo.mp4"_

```shell
# list help
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo --help

# run simple scan
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo demo.mp4

# run full scan
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo -f demo.mp4

# show aspect ratio
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo --Inform="Video;%DisplayAspectRatio%" demo.mp4

# show duration
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo --Inform="General;%Duration/String3%" demo.mp4

# show audio format
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo --Inform="Audio;%Format%" demo.mp4

# show resolution and codec
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo --Inform="Video;Resolution=%Width%x%Height%\nCodec=%CodecID%" demo.mp4

# list all possible file parameters
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo --info-parameters | less

# create XML report (all internal tags)
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo -f --Output=XML demo.mp4

# show mediatrace info
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo --Details=1 demo.mp4

# create report file
$ docker run --rm -i -v ~/Videos/:/mnt debian/mediainfo demo.mp4 --LogFile="Report.log"
```
